import React from "react";
import ReactDOM from "react-dom/client";
import Module from "./components/Module";
import Styled from "./Styled";
import Description from "./components/Description";
import Modal from "./components/Modal";
import "./index.css";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <Module />
    <Description />
    <Modal />
    <Styled />
  </React.StrictMode>
);

