import React from 'react';
import 'antd/dist/antd.css'
import style from './Description.module.css';
import { Descriptions } from 'antd';

const description = () => (
    <div className={style.Description}>
        <Descriptions className={style.Descriptions} title="Player Info" layout="vertical">
            <Descriptions.Item className={style.Descriptions} label="Name">Nilam</Descriptions.Item>
            <Descriptions.Item className={style.Descriptions} label="Username">Ao No Ringo</Descriptions.Item>
            <Descriptions.Item className={style.Descriptions} label="Favorit Hero">Luoyi,kagura,angela</Descriptions.Item>
            <Descriptions.Item className={style.Descriptions} label="Address">Lampung</Descriptions.Item>
        </Descriptions>
    </div>
);
export default description;