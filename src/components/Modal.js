import React, { useState } from 'react';
import { Modal, Button } from 'antd';
import style from './Modal.module.css';

const App = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <div className={style.Modal}>
      <Button type="primary" onClick={showModal} className={style.Modals}>
        Show User
      </Button>
      <Modal title="User Information" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
        <p>Find me at Social Media..</p>
        <p>User Id : 760786624</p>
        <p>Instagram : Nilamcc</p>
        <p>Linkedin : Nilam Cahya</p>
        <p>Facebook : Nilamc</p>
      </Modal>
    </div>
  );
};

export default App